# Ansible Labaratory work #1

## Create the infrastructure
```time ansible-playbook infrastructure.yml -i ./hosts/ec2.py -vvv```

## Installation wordpress
```time ansible-playbook -i hosts/ec2.ini -i hosts/ec2.py site.yml -vvv```

## Delete the whole infrastructure
```time ansible-playbook -i hosts.ini destroy.yml -vvv```
